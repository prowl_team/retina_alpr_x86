#!/bin/bash

if [ ! -d $(pwd)/weights ]; then
  mkdir -p $(pwd)/weights;
fi

wget https://owl-weights.s3.amazonaws.com/yolov3-coco/coco.names -P $(pwd)/weights
wget https://owl-weights.s3.amazonaws.com/yolov3-coco/yolov3.cfg -P $(pwd)/weights
wget https://owl-weights.s3.amazonaws.com/yolov3-coco/yolov3.weights -P $(pwd)/weights

wget https://owl-weights.s3.amazonaws.com/ocr-net/ocr-net.names -P $(pwd)/weights
wget https://owl-weights.s3.amazonaws.com/ocr-net/ocr-net.cfg -P $(pwd)/weights
wget https://owl-weights.s3.amazonaws.com/ocr-net/ocr-net.weights -P $(pwd)/weights

wget https://owl-weights.s3.amazonaws.com/wpod-net/wpod-net_update1.h5 -P $(pwd)/weights
wget https://owl-weights.s3.amazonaws.com/wpod-net/wpod-net_update1.json -P $(pwd)/weights